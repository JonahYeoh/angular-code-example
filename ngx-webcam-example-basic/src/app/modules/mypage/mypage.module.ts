import { NgModule } from '@angular/core';

import { MyPageComponent } from './mypage.component';
import { MyPageRoutingModule } from './mypage-routing.module';
import { CommonModule } from '@angular/common';
import { WebcamModule } from 'ngx-webcam';

@
NgModule
({
  declarations: [
    MyPageComponent
  ],
  imports: [
    CommonModule,
    MyPageRoutingModule,
    WebcamModule
  ],
  exports: [
    MyPageComponent
  ]
})

export class MyPageModule {}

import { Component, OnInit, inject } from '@angular/core';
import { WebcamImage } from 'ngx-webcam';
import { Observable, Subject } from 'rxjs';
@Component({
  selector: 'app-mypage',
  templateUrl: './mypage.component.html',
  styleUrls: ['./mypage.component.css']
})
export class MyPageComponent implements OnInit {
  //
  public webcamImage: WebcamImage | undefined;
  private trigger: Subject<void> = new Subject<void>();
  triggerSnapshot(): void {
    this.trigger.next();
  }
  handleImage(webcamImage: WebcamImage): void {
    console.log('saved webcam image', webcamImage);
    this.webcamImage = webcamImage;
    let wc = document.getElementById("webcam1");
    if (wc) {
      console.log(wc);
      wc.style.display = "none";
    }
    let pp = document.getElementById("previewPanel1");
    if (pp) {
      console.log(pp);
      pp.style.display = "block";
    }
  }

  public get triggerObservable(): Observable<void> {

    return this.trigger.asObservable();
  }

  clearPreview() {
    console.log("Clear Preview");
    let wc = document.getElementById("webcam1");
    if (wc) {
      console.log(wc);
      wc.style.display = "block";
    }
    let pp = document.getElementById("previewPanel1");
    if (pp) {
      pp.style.display = "none";
      console.log(pp);

    }
  }
  //
  ngOnInit(): void {
  }
}
